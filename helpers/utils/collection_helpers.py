"""
Helper functions pour grouper les collections d'Objets

* groupby_attr: Permet de grouper les listes d'Objets Python
    en passant un attribut en Chaines de caractères

* groupby_key: Permet de grouper les list[dict] ou list[list] en passant soit
    un key:str pour les list[dict] ou key:int pour les list[list]
"""
import operator as op
from collections import defaultdict

from .app_utils import dotattrs


def groupby_attr(collection: list, group_attr: str) -> dict:
    """
    Groupe la collection d'objets en un Dict avec les valeurs du group_attr comme Keys

    Args:
        collection: List of objects
        group_attr: L'attribut à utiliser pour regrouper les elements.

    Returns:
        dict: Les 'keys' sont les groupes et les 'values' sont une list[Item].
    """
    # Build a closure function to mimic the behavior of op.attrgetter

    return _groupby(collection, group_attr, dotattrs(group_attr))


def groupby_key(collection: list, group_key: str| int) -> dict:
    """
    Groupe la collection d'objets en un Dict avec les valeurs du group_key comme Keys

    N.B: Dans le cas d'une list[list] group_key peut prendre la valeur d'un Entier (int)

    Args:
        collection: List of dict[]
        group_key: clé à utiliser pour regrouper les elements.

    Returns:
        dict: Les 'keys' sont les groupes et les 'values' une liste du dictionnaire.
    """
    return _groupby(collection, group_key, op.itemgetter(group_key))


def _groupby(collection: list, discriminant: str, disc_getter: callable) -> dict:
    """Regroupe une collection d'objets dans un dictionnaire.

    Cette methode est meilleure que 'itertools.groupby' puisqu'elle ne nécessite
    pas que la collection soit ordonnée selon l'attribut en question.

    Args:
        collection (list): list of objects
        discriminant (str): str representation of grouping attribute.
        disc_getter (callable): a callable to access the attribute of the object.

    Returns:
        dict: a dict with keys as grouping attribute and values as list of objects of the same collection.
        i.e: a = _groupby([{'key0': 'klass1'}, {'key0': 'klass1'}, {'key0': 'klass2'}], 'key0', op.itemgetter)
        a = {'klass1': [{'key0': 'klass1'}, {'key0': 'klass1'}], 'klass2': [{'key0': 'klass2'}]}

    Raises:
        ValueError: if discriminant is not a string.
    """
    if not isinstance(discriminant, (str, int)):
        msg = f"Discriminant must be str ou int, recu: {type(discriminant)}, {discriminant}"
        raise ValueError(msg)

    if callable(disc_getter):
        dico = defaultdict(list)
        for item in collection:
            dico[disc_getter(item)].append(item)

        dico.default_factory = None
        return dico


if __name__ == '__main__':
    pass
