import itertools
import random
import string

import numpy as np


def generate_str_code(code_length: int=4, *, sample_size=100) -> str:
    """Génère un code alphanumérique de N caractères

    Args:
        code_length (int, optional): Size of the codes to generate
        sample_size (int, optional): Description

    Returns:
        str: If as_list is False returns a generator yielding the results
            else: returns a list with all the results
    """
    selection_list = np.array(list(itertools.chain(string.ascii_uppercase, string.digits)))
    matrix = np.random.choice(selection_list, size=(sample_size, code_length))
    rand_str_list = random.choice(matrix)
    str_code = ''.join(rand_str_list)
    return str_code


if __name__ == '__main__':
    sdata = set(generate_str_code(5, sample_size=100) for i in range(100_000))
    print(len(sdata))
