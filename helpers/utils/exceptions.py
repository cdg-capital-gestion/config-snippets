class UtilsError(Exception):
    """Exception générique du dossier Utils. Pour le regroupement des Exceptions"""


class IntervalleError(UtilsError):
    """Intervalle mal défini"""


class EcheancierIntervalleError(IntervalleError):
    """Echeancier mal défini"""


class PeriodeError(UtilsError):
    """Période d'un Echéancier mal spécifiée """


class EcheancierPeriodeError(PeriodeError):
    """Si la Construction de l'Echeancier comporte des Erreurs """


class PlatformDirsError(UtilsError):
    """Exceptions des configurations des dossiers de la plateforme Hexagon"""

