from typing import Any
from pathlib import Path
from datetime import date, datetime
from functools import singledispatch

from .app_utils import to_date, to_datetime, to_yield, to_pip


class Percent(float):
    """Unité en pourcentage"""


class Percent2(float):
    """Unité du jargon financier: Représente "0.01 %", "pips" ou 'pip'"""


@singledispatch
def stringify_datetime_iso_none(value: datetime) -> str:
    return stringify_datetime_iso(value)


@singledispatch
def stringify_date_iso_none(value: date) -> str:
    return stringify_date_iso(value)


@singledispatch
def stringify_path(value: Path):
    return str(value)


@singledispatch
def stringify_csv(value: Any) -> str:
    return str(value)


@singledispatch
def stringify(value: Any) -> str:
    return str(value)


@stringify_path.register
@stringify_csv.register
@stringify.register
def skip_str(value: str):
    return value


@stringify_csv.register
@stringify.register
def stringify_none_empty(value: None) -> str:
    return ''


@stringify_date_iso_none.register
@stringify_datetime_iso_none.register
@stringify_path.register
def stringify_none_none(value: None) -> str:
    return None

@stringify.register
def stringify_float(value: float) -> str:
    return f"{value:,.2f}".replace(',', ' ')


@stringify_csv.register
def stringify_float_csv(value: float) -> str:
    return f"{value:,.2f}".replace(',', ' ').replace('.', ',')


@stringify_csv.register
@stringify.register
def stringify_int(value: int) -> str:
    return f'{value:,}'.replace(',', ' ')


@stringify.register
def stringify_date_iso(value: date) -> str:
    return str(to_date(value))


@stringify.register
def stringify_datetime_iso(value: datetime) -> str:
    return str(to_datetime(value))


@stringify_csv.register
def stringify_date_dmy(value: date) -> str:
    """Retourne une date au format jj/mm/aaaa"""
    return f"{to_date(value):%d/%m/%Y}"


@stringify_csv.register
def stringify_datetime_dmy(value: datetime) -> str:
    """Retourne un datetime au format jj/mm/aaaa hh:mm:ss"""
    return f"{to_datetime(value):%d/%m/%Y %H:%M:%S}"


@stringify_csv.register
@stringify.register
def stringify_percent(value: Percent) -> str:
    return f"{to_yield(value):%}"


@stringify_csv.register
@stringify.register
def stringify_pip(value: Percent2) -> str:
    return f"{to_pip(value):%}"


@stringify_csv.register
@stringify.register
def stringify_bool(value: bool) -> str:
    return str(value)
