# config-snippets
Une collection de fichiers de configuration de fonctions ou portions de code utiles dans la vie d'un projet Python.

## Présentation

Ce projet est intentionnellement publique pour faciliter son exploitation, forking, et pulling dans un nouveau environnement.

### Dossiers Racine
Config-Snippets sera structuré de la manière suivante:

  - configuration: Les fichiers de configuration des outils utilisés (editeurs (sublime), linters (black, flake8, ...), pyproject, hexagon, etc)
  - helpers: Des fonctions utilitaires ayant un périmètre plus large (project level)
  - recipes: Des fonctions recettes réutilisables dans un contexte ponctuel (module level)
        exemples:
          - iterwalker: fonction pour iterer sur une structure imbriquée en utilisant les générateurs. permet aussi d'extraire les objets d'une classe.
  - notebooks: Des jupyter Notebooks interessants à garder pour les profils Data afin d'exploiter rapidement hexagon
  - templates: Divers moèles de fichiers pour divers besoins - Peut contenir des versions neutres de tous les autres dossiers. **Ne pas abuser!**
