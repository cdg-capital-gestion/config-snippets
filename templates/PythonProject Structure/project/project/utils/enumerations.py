from enum import Enum


class Include(Enum):

    """Enumère les inclusions des intervalles au sens mathématique.
    Cette classe est à utiliser wherever we have to define an interval.

    Attributes:
        BOTH (str): Inclut les 2 Bornes [min-max]. Interalle [fermé-fermé]
        LEFT (str): Inclut La Borne gauche [min-max[. Interalle [fermé-ouvert[
        RIGHT (str): Inclut La Borne droite ]min-max]. Interalle ]ouvert-fermé]
        NONE (str): Exclut les 2 Bornes ]min-max[. Interalle ]ouvert-ouvert[
    """

    BOTH = 'BOTH'
    LEFT = 'LEFT'
    RIGHT = 'RIGHT'
    NONE = 'NONE'
