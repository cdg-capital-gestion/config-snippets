import os
import secrets
import hashlib
import platform
import datetime as dt
from collections.abc import Iterable, Iterator
from functools import wraps
from itertools import chain, filterfalse, tee
from math import isclose, isnan, log10

from dateutil import parser

from .exceptions import UtilsError

BASE_MONETAIRE = 360.0
MOIS_LISTE = [
    "Janvier", "Février", "Mars",
    "Avril", "Mai", "Juin",
    "Juillet", "Août", "Septembre",
    "Octobre", "Novembre", "Décembre",
]


def none_sort(iterable: Iterable, *, key: callable=None, reverse=False, none_last=True) -> Iterator:
    """Ordonne une liste d'objets python en prenant en considération Les NoneType.
    se comporte similairement au sorting de python (key and reverse) et groupe les elements
    ayant None soit au début soit vers la fin.

    N.B: La fonction est de Dimension 1 (1D), elle ne classe qu'un element/attribut à la fois
    ex: key=op.itemgetter(1, 3, 5) ou key=op.attrgetter('attr1', 'attr2', 'attr3')

    Args:
        iterable (Iterable): Iterable (tuple, list, set, etc)
        key (callable, None): fonction à appliquer aux objets ex: op.attrgetter, op.itemgetter, etc
        reverse (bool, False): Ordre ascendant par défaut
        none_last (bool, True): Groupe les none vers la fin par défaut, False pour les placer au début

    Returns:
        Iterator: itertools.chain, le choix de la structure de retour reste à la discretion de l'utilisateur
    """
    it1, it2 = tee(iterable, 2)

    if key is None:
        def none_predicate(val):
            return val is None
    else:
        def none_predicate(val):
            return key(val) is None

    not_nones, nones = filterfalse(none_predicate, it1), filter(none_predicate, it2)
    # Met les Nones à la fin
    if none_last:
        return chain(sorted(not_nones, reverse=reverse, key=key), nones)
    else:
        return chain(nones, sorted(not_nones, reverse=reverse, key=key))


def partition(predicate: callable, iterable: Iterable) -> tuple[Iterator]:
    "Use a predicate to partition entries into false entries and true entries"
    # partition(is_odd, range(10)) --> 0 2 4 6 8   and  1 3 5 7 9
    t1, t2 = tee(iterable)
    return filterfalse(predicate, t1), filter(predicate, t2)


def get_mois(adate: dt.date, *, num_mois: int=None) -> str:
    """Retourne le Mois en Français d'une date ou d'un numéro de mois.
    Si num_mois est None prend le mois de la date
    Si num_mois fourni Ne prend pas la date en considération et retourne la représentation du Mois de num_mois

    Args:
        adate (dt.date): Date voulue
        num_mois (int, None): Le numéro du Mois (1=Janvier, 2=Février, 3=Mars, ...)

    Returns:
        str: Représentation du numéro de Mois en Français: 2=Février, 3=Mars

    Raises:
        UtilsError: Si num_mois n'est pas un mois valide
    """
    # Priorité donnée au numéro du Mois (1 pour Janvier)
    if num_mois:
        # Le num doit être un mois VALIDE!
        if not 1 <= num_mois <= 12:
            msg = f"Le {num_mois=} passé n'est pas un numéro de mois valide doit être entre [1..12]"
            raise UtilsError(msg)

        return MOIS_LISTE[num_mois - 1]

    # Utilise la date fournie comme pivot. ix-1 pour python lists
    return MOIS_LISTE[adate.month - 1]


def is_cached(anyfunc: callable) -> bool:
    """Check si la fonction est cachée par functools.lru_cache

    en vérifiant la véracité de tous les attributs:
    ('cache_clear', 'cache_info', 'cache_parameters')

    Args:
        anyfunc (callable): Any function

    Returns:
        bool: True is cached
    """
    if not callable(anyfunc):
        msg = f"L'objet {anyfunc} passé n'est pas une fonction."
        raise UtilsError(msg)

    attrs = ('cache_clear', 'cache_info', 'cache_parameters')
    return all(hasattr(anyfunc, x) for x in attrs)


def reset_cache(anyfunc: callable) -> bool:
    """Rafraichit le cache d'une fonction décorée par lru_cache

    Args:
        anyfunc (callable): Any function

    Returns:
        bool: True si le cache est rafraichi

    Raises:
        UtilsError: Si la fonction n'est pas @lru_cache décorée
    """
    if is_cached(anyfunc):
        anyfunc.cache_clear
        return True

    msg = f"La fonction: {anyfunc} n'est pas une fonction @lru_cache"
    raise UtilsError(msg)


def dotattrs(*args) -> callable:
    """Fonction qui controune les limites de operator.attrgetter.
    Pour les champs ayant un séparateur comme Point('.'), attrgetter considère chaque
    part comme un objet and walks the dotpath: 'titre.classe' ==> obj.titre.classe ce qui n'est pas vrai pour les
    structures à accès hybride comme namedTuple, Row, KeyedTuple et ceux créés par le framework attrs

    Args:
        *args: Liste des attributs à extraire (must be unpacked!)

    Returns:
        callable: Fonction d'accès aux champs définis
    """
    fields = tuple(args)

    def dotgetter(obj: object) -> tuple:
        if len(fields) == 1:
            return getattr(obj, fields[0])

        return tuple(getattr(obj, f) for f in fields)

    # Pour inspection au niveau du Shell Python
    # Liste les champs demandés
    dotgetter.__doc__ = f"Accessor of fields {fields}"
    return dotgetter


def humanize_period(period: dict, separator=':') -> str:
    """Transforme une période 'relativedelta' en une forme humaine
    >>> period = {'days': 15, months': 3, 'years': 2}
    >>> humanize_period(period) => '15D:3M:2Y'

    Args:
        period (dict): Période au sens relativedelta

    Returns:
        str: Période humaine classique {'years': 3} -> '3Y'
    """
    lout = []
    for per, span in period.items():
        out = str(span) + per.upper()[0]
        lout.append(out)

    rv = separator.join(lout)
    return rv


def truncate(value: float, digits: int=0):
    """Tronque le float passé en paramètres à n chiffres après la virgule"""
    svalue = str(value)
    if '.' in svalue:
        entier, decimal = str(value).split('.')
        trunc_value = float('.'.join((entier, decimal[:digits])))
    else:
        trunc_value = float(value)
    return trunc_value


def hexuid(n=16, url_safe=False) -> str:
    """Genere un uid selon l'algo SHA3 256 de 32 caractères .

    >>> a = hexuid() -> 'a7ffc6f8bf1ed76651c14756a061d662'
    >>> len(a) = 32

    Returns:
        str: a string of 32 characters.
    """
    if url_safe:
        return secrets.token_urlsafe(n)
    
    return secrets.token_hex(n)


def hexdigest(str_value: str, string_size: int=32) -> str:
    """Genere un uid selon l'algo SHA3 256 de 'string_size' depuis le début.

    >>> a = hexdigest('Mero89') = 'a7ffc6f8bf1ed76651c14756a061d662'
    >>> len(a) = string_size

    Returns:
        str: a string of 32 characters.

    Args:
        str_value (str): Chaine en input
        string_size (int, optional=32): la taille de la chaine de caractères.

    Raises:
        UtilsError: Si la variable n'est pas str, ou impossible à décoder.
    """
    try:
        value = str(str_value)
        hasher = hashlib.sha3_256(value.decode('utf-8'))
        digest = hasher.hexdigest()

        if len(digest) < string_size:
            return digest

        return digest[:string_size]

    except (TypeError, UnicodeError) as err:
        print(err)
        raise UtilsError('le text passé ne peut être Hashé')


def current_user(upper: bool=True) -> str:
    """Retourne le nom d'utilisateur de la session: os.getlogin()

    Returns:
        str: User Name as in os.getlogin().upper()
    """
    try:
        uname = os.getlogin()

    except OSError:
        uname = 'Hexagon-Core'

    if upper:
        return uname.upper()

    return uname


def current_machine(upper: bool=True) -> str:
    """Fournit le nom de la machine: os.environ['COMPUTERNAME']

    Returns:
        str: Computer Name as in platform.
    """
    machine = platform.node()
    if upper:
        return machine.upper()

    return machine


def current_platform(upper: bool=True) -> str:
    """Retourne la plateforme de l'Application, utlise platform.system()

    Args:
        upper (bool, optional): Si True, retourne le système en MAJUSCULES
    """
    plat = platform.system()

    if upper:
        return plat.upper()

    return plat


def timestamp() -> dt.datetime:
    """Retourne le timestamp"""
    now = dt.datetime.now()
    return now


def arrondi(digits: int) -> float:
    """Génère une fonction décoratrice qui arrondit à 'n' chiffres après

    la virgule le résultat de la fonction décorée.

    >>> @arrondi(2)  # contruit une fonction décoratrice à 2 chiffres après la virgule
    >>> def myfunc():
    >>>     return 3.456
    >>>
    >>> rv = myfunc()
    >>> rv = 3.46

    Args:
        digits (int): Nombre de décimales à arrondir

    Returns:
        callable: la fonction d'arrondi générée et paramétrée à n chiffres
    """

    def arrondi_n(func, digits=digits):
        @wraps(func)
        def wrapped(*args, **kwargs):
            try:
                return round(func(*args, **kwargs), digits)

            except TypeError:
                return round(float(func(*args, **kwargs)), digits)

        return wrapped
    return arrondi_n


def base_a(_date: dt.date, step: int=0) -> int:
    """Retourne la base annuelle d'une date ou une date future en passant le nombre de jours step

    Cette fonction est utilisée pour la détermination de la base annuelle des coupons des obligations.
    """
    # Un coupon dans 400 jours => step = 400
    date_ref_step = _date + dt.timedelta(step)
    try:
        prev_year = date_ref_step.replace(year=date_ref_step.year-1)

    except ValueError:
        prev_year = date_ref_step.replace(year=date_ref_step.year-1, day=date_ref_step.day-1)

    # Si un 29 fevrirer est inclus, la base sera de 366
    diff = (date_ref_step - prev_year).days
    return diff


def to_date(value: str) -> dt.date:
    """Retourne une date depuis un string, format dd/mm/yyyy ou dd-mm-yyyy """
    if value in (None, '', 'None'):
        return None

    if isinstance(value, dt.datetime):
        return value.date()

    if isinstance(value, dt.date):
        return value

    if isinstance(value, str):
        sep = ' '
        dayfirst = True

        if '-' in value:
            sep = '-'
        elif '/' in value:
            sep = '/'

        first, *sp_value = value.split(sep)

        if int(first) > 31:
            dayfirst = False

        return parser.parse(value, dayfirst=dayfirst).date()

    else:
        return value


def to_datetime(value: str) -> dt.datetime:
    """
    Retourne un datetime depuis un string, format dd/mm/yyyy ou dd-mm-yyy
    """
    if value in (None, '', 'None'):
        return None

    if isinstance(value, dt.datetime):
        return value

    if isinstance(value, dt.date):
        rv = dt.datetime.fromisoformat(str(value))
        return rv

    if isinstance(value, str):
        sep = ' '
        dayfirst = True

        if '-' in value:
            sep = '-'

        elif '/' in value:
            sep = '/'

        first, *sp_value = value.split(sep)
        if int(first) > 31:
            dayfirst = False

        return parser.parse(value, dayfirst=dayfirst)

    else:
        return value


def to_bool(value: str) -> bool:
    """Convertit une cdc en booléen selon un mapping simple

    >>> to_bool('Y') -> True
    >>> to_bool('O') -> True

    Args:
        value (str): Une valeur décrivant un état booléan

    Returns:
        bool: True ou False
    """
    svalue = str(value)

    dico = {
        1: True, '1': True,
        'o': True, 'oui': True,
        'vrai': True, 'true': True,
        'y': True, 'yes': True,

        0: False, '0': False,
        'n': False, 'non': False,
        'faux': False,
        'false': False, 'no': False,
        'none': False, 'null': False,
    }

    rv = dico.get(svalue.lower(), None)
    return rv


def to_yield(yield_value: float) -> float:
    """Valide une valeur en pourcentage.

    >>> to_yield(2.5) = 0.025  # 2.5 %

    Args:
        yield_value (float): un pourcentage

    Returns:
        float: Le pourcentage en unité naturelle: 2.5% = 0.025
    """
    isneg = 1
    try:
        fvalue = float(yield_value)
        if fvalue == 0:
            return 0

        if fvalue < 0:
            isneg = -1
            fvalue = abs(fvalue)

        if log10(fvalue) >= 0:
            fvalue /= 100

        return fvalue * isneg

    except (TypeError, ValueError):
        # print('to_yield:\n', err)
        return None


def to_pip(pip_value: float) -> float:
    """Valide une valeur en points de base (pb).

    >>> to_pip(80) = 0.008  # 0.8%

    Args:
        pip_value (float): Un taux/spread exprimé en point de base ou pip

    Returns:
        float: Le spread en unité naturelle: 1pb = 1e-4
    """
    isneg = 1
    try:

        fvalue = float(pip_value)
        if fvalue == 0:
            return 0

        if fvalue < 0:
            isneg = -1
            fvalue = abs(fvalue)

        if log10(fvalue) >= -1:
            fvalue = fvalue / 10000

        return fvalue * isneg

    except (TypeError, ValueError):
        # print(f"to_pip: {str(err)}")
        return None


def to_float(float_value: str) -> float:
    """Valide un texte qui représente un nombre rationnel.


    >>> float('2.5') = 2.50  # float

    Args:
        float_value (str): nombre en texte

    Returns:
        float: Le nombre en unité de base
    """
    try:
        fvalue = float(float_value)
        return fvalue

    except (TypeError, ValueError):
        if isinstance(float_value, str):
            stripped = float_value.strip()
            zero_str = ('NC', '-', '')
            if stripped in zero_str:
                return 0

        return None


def to_int(int_value: str) -> int:
    """Valide un texte qui représente un nombre entier.

    Args:
        int_value (str): nombre entier en texte

    Returns:
        int: Le nombre entier en unité de base
    """
    try:
        ivalue = int(int_value)
        return ivalue

    except (TypeError, ValueError):
        # print(f"to_int: {str(err)}")
        return None


def validate_methode_valo(value: str) -> str:
    if isinstance(value, str):
        val = value.upper()

        if val in ('FIX', 'N', 'F', 'ATYP', 'AT', 'FIXE', 'NORMAL'):
            return 'N'

        elif val in ('REV', 'REVISABLE', 'VARIABLE'):
            return 'REV'

        else:
            return val
    else:
        return value


def check_periodicite_titre(value: str) -> str:
    """Vérifie si la périodicité du titre est permise.

    Retourne un des choix suivants:
        (A: Annuelle, S: Semestrielle, T: Trimestrielle, M: Mensuelle, B: BiHebdo - 15 jours)

    Args:
        value (str): Périodicité en str

    Returns:
        str: code périodicité 'CHOIX_PERIODICITE'

    Raises:
        ValueError: Si la périodicité n'est pas permise
    """
    if isinstance(value, str):
        val = value.upper()
        CHOIX_PERIODICITE = ('A', 'T', 'S', 'M', 'B')

        if len(val) == 1 and val in CHOIX_PERIODICITE:
            return val
        val_start = val[0]

        if val_start in CHOIX_PERIODICITE:
            return CHOIX_PERIODICITE[CHOIX_PERIODICITE.index(val_start)]

        else:
            header_msg = "Periodicite du titre doit etre l'un des choix suivants:\n"
            opts = "; ".join(CHOIX_PERIODICITE)
            msg = f"{header_msg} {opts}"
            raise ValueError(msg)

    msg = f"Type de la Périodicité incorrect: {type(value)}"
    raise ValueError(msg)


def upper_string(str_value: str) -> str:
    """Supprime les espaces aux extrémités et transforme en Majuscule une chaine de caractères (CdC)

    Args:
        str_value (str): CdC à transformer en Upper Case

    Returns:
        str: Upper Cased String, Chaine en Majuscule
    """
    if isinstance(str_value, str):
        ustring = str_value.strip().upper()
        return ustring

    else:
        return str_value


def lower_string(str_value: str) -> str:
    """Supprime les espaces aux extrémités et transforme en Minuscule une chaine de caractères (CdC)

    Args:
        str_value (str): CdC à transformer en Lower Case

    Returns:
        str: Lower Cased String, Chaine en Minuscule
    """
    if isinstance(str_value, str):
        lstring = str_value.strip().lower()
        return lstring

    else:
        return str_value
