from enum import Enum, auto


class Choix(Enum):
    BLANC = 'BLANC'
    NOIR = 'NOIR'
